#!/bin/sh

set -eu

base=$(readlink -f "$(dirname "$(readlink -f "$0")")/../..")
. "$base/lib/environment.sh"

prepare_args
rootfs="$1"

# convert apt proxy pointing to the VM's host back to localhost
proxy_opt=
RES=$(chroot "${rootfs}" apt-config shell PROXY Acquire::http::Proxy)
eval "$RES"
if [ -n "${PROXY:-}" ]; then
  proxy_opt="-o Acquire::http::Proxy=$(echo "${PROXY}" | sed -e 's/10.0.2.2/127.0.0.1/')"
fi
# shellcheck disable=SC2086
DEBIAN_FRONTEND=noninteractive \
  chroot "$rootfs"  \
  apt-get install $proxy_opt dpkg-dev ca-certificates -q -y --no-install-recommends
if chroot "$rootfs" apt-cache show auto-apt-proxy >/dev/null 2>&1; then
  # shellcheck disable=SC2086
  DEBIAN_FRONTEND=noninteractive \
    chroot "$rootfs"  \
    apt-get install $proxy_opt auto-apt-proxy -q -y --no-install-recommends
fi

DEBIAN_FRONTEND=noninteractive \
  chroot "$rootfs"  \
  apt-get clean

chroot "$rootfs"  \
  useradd \
    --home /home/debci \
    --create-home \
    debci
