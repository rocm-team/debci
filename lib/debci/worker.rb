require 'debci/db'

module Debci
  class Worker < ActiveRecord::Base
    has_many :jobs, class_name: 'Debci::Job'
  end
end
